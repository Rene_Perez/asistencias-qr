// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDf1e4ti-9jqfheFoUZRsOlHbp4cjmAhwg',
  authDomain: 'asistenciaqr-1add0.firebaseapp.com',
  databaseURL: 'https://asistenciaqr-1add0.firebaseio.com',
  projectId: 'asistenciaqr-1add0',
  storageBucket: 'asistenciaqr-1add0.appspot.com',
  messagingSenderId: '269855018452',
  appId: '1:269855018452:web:5eb631170f5e1d76'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
