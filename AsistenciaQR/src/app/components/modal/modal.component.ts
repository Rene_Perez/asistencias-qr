import { UserInterface } from './../../models/user';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { AuthService } from '../../services/auth.service';
import {Alumnos} from '../../models/listarAlumnos/alumnos';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(private dataApi: DataApiService, private authService: AuthService) { }
  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;
  ngOnInit() {
  }

  onSaveUser(userForm: NgForm): void {
    if (userForm.value.id == null) {
      // New 
      userForm.value.userUid = this.userUid;
      this.dataApi.addUser(userForm.value);
    } else {
      // Update
      this.dataApi.updateUser(userForm.value);
    }
    userForm.resetForm();
    this.btnClose.nativeElement.click();
  }

}
