import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserInterface } from './../../models/user';
import * as jsPDF from 'jspdf';
import { from } from 'rxjs';
import { Source } from 'webpack-sources';
import { $ } from 'protractor';
import { Script } from 'vm';
import { ListaInterface } from 'src/app/models/lista';
import { DataApiService } from 'src/app/services/data-api.service';

@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.css']
})
export class InformesComponent implements OnInit {

   @ViewChild('content') content: ElementRef;
  constructor(private dataApi: DataApiService) { }

  public listas: ListaInterface[];

  ngOnInit() {
    this.dataApi.getAllListas().subscribe(listas => {
      console.log('LISTAS', listas);
      this.listas = listas;
    })
  }

public descargarpdf()
{
  let doc = new jsPDF();
  let specialEvent ={
    '#editor': function(element: any,rederer: any){
      return true;
    }
  };

  let content = this.content.nativeElement;

  doc.fromHTML(content.innerHTML,10,10,{

    'widht': 10,
    'elementHandlers': specialEvent
  });

  doc.save('asd.pdf');

}
  /*descargarpdf(){
    const doc = new jsPDF();
    doc.text('algo xD',10,10);
    doc.save('test.pdf');
    var xepOnline:any
    return xepOnline
  }*/

}

