import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { Permiso } from 'src/app/models/permisos/permiso';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.css']
})
export class PermisosComponent implements OnInit {

  constructor(private dataApi: DataApiService, private authService: AuthService) { }
  private permisos: Permiso[];
  public isAdmin: any = null;
  public userUid: string = null;

  ngOnInit() {
    this.getListpermisos();
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          // this.isAdmin = true;
        })
      }
    })
  }
  onSaveAlumno(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New 
      bookForm.value.userUid = this.userUid;
      this.dataApi.addPermiso(bookForm.value);
    } else {
      // Update
      //this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    //this.btnClose.nativeElement.click();
  }
  
  getListpermisos() {
    this.dataApi.getAllPermiso()
      .subscribe(permisos => {
        this.permisos = permisos;
        console.log(this.permisos);
      });
  }

  /*onDeleteBook(idBook: string): void {
    const confirmacion = confirm('Are you sure?');
    if (confirmacion) {
      this.dataApi.deleteBook(idBook);
    }
  }

  onPreUpdateBook(book: Permiso) {
    console.log('BOOK', book);
    this.dataApi.selectedBook = Object.assign({}, book);
  }*/


}
