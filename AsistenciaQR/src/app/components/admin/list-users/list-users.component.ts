import { UserInterface } from './../../../models/user';
import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  constructor(private dataApi: DataApiService, private authService: AuthService) { }
  public users: UserInterface[]
    
  
  
  public isAdmin: any = null;
  public userUid: string = null;

  ngOnInit() {
    this.getListUsers();
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          // this.isAdmin = true;
        });
      }
    });
  }
  getListUsers() {
    this.dataApi.getAllUsers()
      .subscribe(users => {
        this.users = users;
      });
  }

  onDeleteUser(idUser: string): void {
    const confirmacion = confirm('Are you sure?');
    if (confirmacion) {
      this.dataApi.deleteUser(idUser);
    }
  }

  onPreUpdateUser(user: UserInterface) {
    console.log('USER', user);
    this.dataApi.selectedUser = Object.assign({}, user);
  }
}
