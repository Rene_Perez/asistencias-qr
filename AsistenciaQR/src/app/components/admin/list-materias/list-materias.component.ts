import { GestionInterface } from '../../../models/gestion';
import { Component, OnInit } from '@angular/core';
import { DataApiService } from 'src/app/services/data-api.service';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-list-materias',
  templateUrl: './list-materias.component.html',
  styleUrls: ['./list-materias.component.css']
})
export class ListMateriasComponent implements OnInit {

  constructor(private dataApi: DataApiService, private authService: AuthService) { }
  private gestiones: GestionInterface[];
  public isAdmin: any = null;
  public userUid: string = null;
  public carr: 'no';
  ngOnInit() {
    this.getListGestiones();
    this.getCurrentUser();
  }
  onSaveGestion(gestionForm: NgForm): void {
    console.log(gestionForm.value.id);
    if (gestionForm.value.id == null) {
      // New
      gestionForm.value.userUid = this.userUid;
      this.dataApi.addGestion(gestionForm.value);
    } else {
      // Update
      this.dataApi.updateGestion(gestionForm.value);
    }
    gestionForm.resetForm();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          // this.isAdmin = true;
        });
      }
    });
  }
  getListGestiones() {
    this.dataApi.getAllGestiones()
      .subscribe(gestiones => {
        this.gestiones = gestiones;
      });
  }

  onDeleteGestion(idGestion: string): void {
    const confirmacion = confirm('Are you sure?');
    if (confirmacion) {
      this.dataApi.deleteGestion(idGestion);
    }
  }
}
