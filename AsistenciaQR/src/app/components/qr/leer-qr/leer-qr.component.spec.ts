import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeerQRComponent } from './leer-qr.component';

describe('LeerQRComponent', () => {
  let component: LeerQRComponent;
  let fixture: ComponentFixture<LeerQRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeerQRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeerQRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
