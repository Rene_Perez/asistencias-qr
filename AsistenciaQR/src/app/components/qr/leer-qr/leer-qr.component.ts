import { Permiso } from './../../../models/permisos/permiso';
import { ListaInterface } from './../../../models/lista';
import {Component, ViewChild, ViewEncapsulation, OnInit} from '@angular/core';
import {QrScannerComponent} from 'angular2-qrscanner';
import { DataApiService } from 'src/app/services/data-api.service';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { timestamp } from 'rxjs/operators';
import { formatNumber, DatePipe, formatDate } from '@angular/common';
import { GestionInterface } from 'src/app/models/gestion';

@Component({
  selector: 'app-leer-qr',
  templateUrl: './leer-qr.component.html',
  styleUrls: ['./leer-qr.component.css']
})
export class LeerQRComponent implements OnInit {
asistenciaQR: string;
estado: string;
listaAsistencias: string[];
index: number;
fecha: Date;
  @ViewChild(QrScannerComponent) qrScannerComponent: QrScannerComponent ;

  constructor(private dataApi: DataApiService, private authService: AuthService) { }
  private gestiones: GestionInterface[];
  private listas: ListaInterface[];
  public isAdmin: any = null;
  public userUid: string = null;


  ngOnInit() {
        this.getListListas();
        this.getCurrentUser();
        this.dataApi.getAllGestiones().subscribe(gestiones => {
          console.log('GESTIONES', gestiones);
          this.gestiones = gestiones;
        })
        this.qrScannerComponent.getMediaDevices().then(devices => {
          console.log(devices);
          const videoDevices: MediaDeviceInfo[] = [];
          for (const device of devices) {
              if (device.kind.toString() === 'videoinput') {
                  videoDevices.push(device);
              }
          }
          if (videoDevices.length > 0) {
              let choosenDev;
              for (const dev of videoDevices) {
                  if (dev.label.includes('front')) {
                      choosenDev = dev;
                      break;
                  }
              }
              if (choosenDev) {
                  this.qrScannerComponent.chooseCamera.next(choosenDev);
              } else {
                  this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
              }
          }
      });

        this.qrScannerComponent.capturedQr.subscribe(result => {
          console.log(result);
          this.asistenciaQR = result;
          this.fecha = new Date(Date.now());
          this.estado = 'Atrasado';
      });
  }

  onSaveLista(listaForm: NgForm): void {
    console.log(listaForm.value.id);
    if (listaForm.value.id == null) {
      // New
      listaForm.value.userUid = this.userUid;
      this.dataApi.addLista(listaForm.value);
    } else {
      // Update
      this.dataApi.updateLista(listaForm.value);
    }
    listaForm.resetForm();
  }
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
        });
      }
    });
  }
  getListListas() {
    this.dataApi.getAllListas()
      .subscribe(listas => {
        this.listas = listas;
      });
  }
  onPreUpdateLista(lista: ListaInterface) {
    console.log('LISTA', lista);
    this.dataApi.selectedLista = Object.assign({}, lista);
  }
}
