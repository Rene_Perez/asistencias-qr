import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { UserInterface } from '../../../models/user';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-generar-qr',
  templateUrl: './generar-qr.component.html',
  styleUrls: ['./generar-qr.component.css']
})
export class GenerarQRComponent implements OnInit {

  constructor(private authService: AuthService) {
    this.fecha = new Date(Date.now());
    setInterval(() => {this.send2Server(); }, 60000);
   }
  // aqui ponemos los datos que queremos que se generen
  CodigoQRDelUsuario = '';
  fecha: Date;
  myDate: Date;

  user: UserInterface = {
    email: '',
    photoUrl: '',

    roles: {}
  };

  public providerId = 'null';
  send2Server() {
    this.authService.isAuth().subscribe(user => {
      if (user) {
        this.CodigoQRDelUsuario = user.email + ' fecha: ' + this.fecha ;
      }
    });
  }
  ngOnInit() {
    this.send2Server();
  }
}
