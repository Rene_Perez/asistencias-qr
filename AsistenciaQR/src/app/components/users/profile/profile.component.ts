import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { UserInterface } from '../../../models/user';
import { DataApiService } from '../../../services/data-api.service';



import {NgForm} from '@angular/forms';

import {AlumnosService} from '../../../services/listarAlumnos/alumnos.service';
import { from } from 'rxjs';
import { Alumnos } from 'src/app/models/listarAlumnos/alumnos';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private authService: AuthService, private route: ActivatedRoute, private dataApi: DataApiService) {
   }
  private users: UserInterface[];
  public isAdmin: any = null;
  public userUid: string = null;
  CodigoQRDelUsuario = '';
  user: UserInterface = {
    email: '',
    photoUrl: '',
    roles: {}
  };

  public providerId = 'null';
  ngOnInit() {
    this.getListUsers();
    this.getCurrentUser();
    this.authService.isAuth().subscribe(user => {
      if (user) {

        this.user.codUser = user.displayName;

        this.user.nombres = user.displayName;


        this.user.email = user.email;
        this.user.photoUrl = user.photoURL;
        this.CodigoQRDelUsuario = user.displayName;
        this.providerId = user.providerData[0].providerId;
      }
    });
  }
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserAdmin(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          // this.isAdmin = true;
        });
      }
    });
  }
  getListUsers() {
    this.dataApi.getAllUsers()
      .subscribe(users => {
        this.users = users;
      });
  }

  onDeleteUser(idUser: string): void {
    const confirmacion = confirm('Are you sure?');
    if (confirmacion) {
      this.dataApi.deleteUser(idUser);
    }
  }

  onPreUpdateUser(user: UserInterface) {
    console.log('USER', user);
    this.dataApi.selectedUser = Object.assign({}, user);
  }
}
