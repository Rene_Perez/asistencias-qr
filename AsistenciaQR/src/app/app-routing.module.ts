import { ListMateriasComponent } from './components/admin/list-materias/list-materias.component';
import { ListUsersComponent } from './components/admin/list-users/list-users.component';
import { PermisosComponent } from './components/permisos/permisos.component';
import { ListAlumnosComponent } from './components/admin/list-alumnos/list-alumnos.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from 'src/app/components/users/login/login.component';
import { RegisterComponent } from 'src/app/components/users/register/register.component';
import { ProfileComponent } from 'src/app/components/users/profile/profile.component';
import { Page404Component } from './components/page404/page404.component';
import { AuthGuard } from './guards/auth.guard';
import { LeerQRComponent } from './components/qr/leer-qr/leer-qr.component';
import { GenerarQRComponent } from './components/qr/generar-qr/generar-qr.component';
import { InformesComponent } from './components/informes/informes.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'user/login', component: LoginComponent },
  { path: 'user/register', component: RegisterComponent },
  { path: 'user/profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'admin/list-alumnos', component: ListAlumnosComponent, canActivate: [AuthGuard] },
  { path: 'admin/list-users', component: ListUsersComponent, canActivate: [AuthGuard] },
  { path: 'admin/list-materias', component: ListMateriasComponent, canActivate: [AuthGuard] },
  { path: 'permisos', component: PermisosComponent, canActivate: [AuthGuard] },
  { path: 'informes', component: InformesComponent, canActivate: [AuthGuard] },
  { path: 'qr/generarQR', component: GenerarQRComponent, canActivate: [AuthGuard] },
  { path: 'qr/leerQR', component: LeerQRComponent, canActivate: [AuthGuard] },
  { path: '**', component: Page404Component }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
