import { Injectable } from '@angular/core';


import{AngularFireDatabase,AngularFireList} from '@angular/fire/database';

import { from } from 'rxjs';
import { Permiso } from 'src/app/models/permisos/permiso';

@Injectable({
  providedIn: 'root'
})
export class PermisoService {

  permisosList: AngularFireList<any>
  //selectPersona: Permiso = new Permiso();
  constructor(private firebase:AngularFireDatabase) { }

  getPermiso(){
    this.permisosList= this.firebase.list('permisos')
  }
  insertPermiso(permisos:Permiso){
    this.permisosList.push({
      namefull: permisos.namefull,
      ci: permisos.ci,
      semestre: permisos.semestre,
      celular: permisos.celular,
      carrera: permisos.carrera,

      lisenciaasista: permisos.licesiaasistencia,
      lisenciaex:permisos.lisenciaexamen,
      lisenciamateria: permisos.licesiatutorial,
      lisenciatraba: permisos.licesiatrabajo,


      materia: permisos.materia,
      docente: permisos.docente,


      justificacion: permisos.justificacion
      
    });
  }
  updatePermiso(permisos:Permiso){
    this.permisosList.update(permisos.id, {
      namefull: permisos.namefull,
      ci: permisos.ci,
      semestre: permisos.semestre,
      celular: permisos.celular,
      carrera: permisos.carrera,

      lisenciaasista: permisos.licesiaasistencia,
      lisenciaex:permisos.lisenciaexamen,
      lisenciamateria: permisos.licesiatutorial,
      lisenciatraba: permisos.licesiatrabajo,


      materia: permisos.materia,
      docente: permisos.docente,


      justificacion: permisos.justificacion
    });
  }

  deletePersona(id:string){
    this.permisosList.remove(id)
  }
}
