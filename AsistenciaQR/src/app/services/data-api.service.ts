import { GestionInterface } from './../models/gestion';
import { ListaInterface } from './../models/lista';
import { UserInterface } from './../models/user';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Permiso } from '../models/permisos/permiso';
@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private afs: AngularFirestore) { }

  private usersCollection: AngularFirestoreCollection<UserInterface>;
  private users: Observable<UserInterface[]>;
  private userDoc: AngularFirestoreDocument<UserInterface>;
  private user: Observable<UserInterface>;
  public selectedUser: UserInterface = {
    id: '',
    nombres: '',
    carrera: '',
    email: ''
  };
  private listasCollection: AngularFirestoreCollection<ListaInterface>;
  private listas: Observable<ListaInterface[]>;
  private listaDoc: AngularFirestoreDocument<ListaInterface>;
  private lista: Observable<ListaInterface>;
  public selectedLista: ListaInterface = {
    id: null
  };
  private gestionesCollection: AngularFirestoreCollection<GestionInterface>;
  private gestiones: Observable<GestionInterface[]>;
  private gestionDoc: AngularFirestoreDocument<GestionInterface>;
  private gestion: Observable<GestionInterface>;
  public selectedGestion: GestionInterface = {
    id: null
  };
  /*permisos*/
  private AlumnoColleciton: AngularFirestoreCollection<Permiso>;
  private permisos: Observable<Permiso[]>;
  private permisoDoc: AngularFirestoreDocument<Permiso>;
  private permiso: Observable<Permiso>;
  public selectedPermiso: Permiso = {
    id: null,
    namefull: '',
    ci: '',
    celular: '',
    semestre: '',
    carrera: '',
    licesiaasistencia: '',
    lisenciaexamen: '',
    licesiatutorial: '',
    licesiatrabajo: '',
    materia: '',
    docente: '',
    justificacion: ''
  };
  // Gestiones
  getAllGestiones() {
    this.gestionesCollection = this.afs.collection<GestionInterface>('gestiones');
    return this.gestiones = this.gestionesCollection.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as GestionInterface;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
    }
  addGestion(gestion: GestionInterface): void {
    this.gestionesCollection.add(gestion);
  }
  updateGestion(gestion: GestionInterface): void {
    const idGestion = gestion.id;
    this.gestionDoc = this.afs.doc<GestionInterface>(`gestiones/${idGestion}`);
    this.gestionDoc.update(gestion);
  }
  deleteGestion(idGestion: string): void {
    this.gestionDoc = this.afs.doc<GestionInterface>(`gestiones/${idGestion}`);
    this.gestionDoc.delete();
  }
// Usuarios
  getAllUsers() {
    this.usersCollection = this.afs.collection<UserInterface>('users');
    return this.users = this.usersCollection.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as UserInterface;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
    }
  addUser(user: UserInterface): void {
    this.usersCollection.add(user);
  }
  updateUser(user: UserInterface): void {
    const idUser = user.id;
    this.userDoc = this.afs.doc<UserInterface>(`users/${idUser}`);
    this.userDoc.update(user);
  }
  deleteUser(idUser: string): void {
    this.userDoc = this.afs.doc<UserInterface>(`users/${idUser}`);
    this.userDoc.delete();
  }
  /*Listas*/
  getAllListas() {
    this.listasCollection = this.afs.collection<ListaInterface>('listas');
    return this.listas = this.listasCollection.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as ListaInterface;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
    }
  addLista(lista: ListaInterface): void {
    this.listasCollection.add(lista);
  }
  updateLista(lista: ListaInterface): void {
    const idLista = lista.id;
    this.listaDoc = this.afs.doc<ListaInterface>(`listas/${idLista}`);
    this.listaDoc.update(lista);
  }

  getAllPermiso() {
    this.AlumnoColleciton = this.afs.collection<Permiso>('permiso');
    return this.permisos = this.AlumnoColleciton.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as Permiso;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
  }


 /* getAllBooksOffers() {
    this.AlumnoColleciton = this.afs.collection('books', ref => ref.where('oferta', '==', '1'));
    return this.permisos = this.AlumnoColleciton.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as Permiso;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
  }*/

  getOnePermiso(idPermiso: string) {
    this.permisoDoc = this.afs.doc<Permiso>(`permiso/${idPermiso}`);
    return this.permiso = this.permisoDoc.snapshotChanges().pipe(map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Permiso;
        data.id = action.payload.id;
        return data;
      }
    }));
  }

  addPermiso(permiso: Permiso): void {
    this.AlumnoColleciton.add(permiso);
  }
  updatePermiso(permiso: Permiso): void {
    const idPermiso = permiso.id;
    this.permisoDoc = this.afs.doc<Permiso>(`permiso/${idPermiso}`);
    this.permisoDoc.update(permiso);
  }
  deletePermiso(idPermiso: string): void {
    this.permisoDoc = this.afs.doc<Permiso>(`permiso/${idPermiso}`);
    this.permisoDoc.delete();
  }
}
