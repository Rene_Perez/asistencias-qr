import { Injectable } from '@angular/core';

import{AngularFireDatabase,AngularFireList} from '@angular/fire/database';
import { Alumnos } from 'src/app/models/listarAlumnos/alumnos';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  AlumnosList: AngularFireList<any>
  selectAlumno: Alumnos = new Alumnos();
  constructor(private firebase:AngularFireDatabase) { }

  getAlumno(){
    this.AlumnosList= this.firebase.list('alumnos')
  }
  insertAlumno(alumnos:Alumnos){
    this.AlumnosList.push({
      names: alumnos.names,
      primerapellido: alumnos.primerapellido,
      segunapellido:alumnos.segundoapellido,
      carrera: alumnos.carrera,
      semestre:alumnos.semestre,
      direccion: alumnos.materia,
      facultad: alumnos.facultad
     
    });
  }
  updateAlumno(alumnos:Alumnos){
    this.AlumnosList.update(alumnos.id, {
      names: alumnos.names,
      primerapellido: alumnos.primerapellido,
      segunapellido:alumnos.segundoapellido,
      carrera: alumnos.carrera,
      semestre:alumnos.semestre,
      direccion: alumnos.materia,
      telefono: alumnos.facultad
    });
  }

  deleteAlumno($key:string){
    this.AlumnosList.remove($key)
  }
}
