import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroComponent } from './components/hero/hero.component';
import { HomeComponent } from './components/home/home.component';
import { ModalComponent } from './components/modal/modal.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/users/login/login.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { RegisterComponent } from './components/users/register/register.component';
import { Page404Component } from './components/page404/page404.component';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { ListAlumnosComponent } from './components/admin/list-alumnos/list-alumnos.component';
import { LeerQRComponent } from './components/qr/leer-qr/leer-qr.component';
import { GenerarQRComponent } from './components/qr/generar-qr/generar-qr.component';

import {NgxQRCodeModule} from 'ngx-qrcode2';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { InformesComponent } from './components/informes/informes.component';
import { PermisosComponent } from './components/permisos/permisos.component';
import { DatosComponent } from './components/user/datos/datos.component';
import { ListUsersComponent } from './components/admin/list-users/list-users.component';
import { ModalUsersComponent } from './components/modal-users/modal-users.component';
import { ListMateriasComponent } from './components/admin/list-materias/list-materias.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroComponent,
    HomeComponent,
    ModalComponent,
    NavbarComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    Page404Component,
    ListAlumnosComponent,
    LeerQRComponent,
    GenerarQRComponent,
    InformesComponent,
    PermisosComponent,
    DatosComponent,
    ListUsersComponent,
    ModalUsersComponent,
    ListMateriasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    NgxQRCodeModule,
    NgQrScannerModule
  ],
  providers: [AngularFireAuth, AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
