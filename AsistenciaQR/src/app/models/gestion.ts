export interface GestionInterface {
    id?: string;
    userUid?: string;

    gestion?: string;
    facultad?: Facultad;
  }
export interface Facultad {
    nombreFacultad?: string;
    semestre?: Semestre;
  }

export interface Semestre {
    semestre?: string;
    carrera?: Carrera;
  }

export interface Carrera {
    nombreCarrera?: string;
    materia?: Materia;
  }

export interface Materia {
    nombreMateria?: string;
    horaEntrada?: string;

  }
