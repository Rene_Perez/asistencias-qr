export interface Roles {

  administrador?: boolean;

  admin?: boolean;
}

export interface UserInterface {
  id?: string;

  photoUrl?: string;

  codUser?: string;

  carrera?: string;

  email?: string;
  password?: string;

  nombres?: string;
  primerApellido?: string;
  segundoApellido?: string;
/*
  facultad?: Facultad;
  carrera?: Carrera;
  materia?: Materia;
*/
  roles?: Roles;
}
