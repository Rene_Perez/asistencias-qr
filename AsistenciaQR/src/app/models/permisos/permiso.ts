
export interface Permiso {
    id?:string;
    namefull: string;
    ci: string;
    celular:string;
    semestre: string;
    carrera:string;

    licesiaasistencia:string;
    lisenciaexamen:string;
    licesiatutorial:string;
    licesiatrabajo:string;

    materia:string;
    docente:string;

    justificacion:string;

}
