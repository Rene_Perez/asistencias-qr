export interface ListaInterface {
    id?: string;
    userUid?: string;

    estudiante?: string;
    materia?: string;
    horaEntrada?: string;
    estado?: string;
  }
